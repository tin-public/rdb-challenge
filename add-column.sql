ALTER TABLE `product` ADD COLUMN `product_dup_id` INT NULL AFTER `price`;
SET SQL_SAFE_UPDATES=0;
UPDATE `product` set `product_dup_id` = `product_id`;
SET SQL_SAFE_UPDATES=1;
