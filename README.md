# RDB challenge

## Create environment
Run `docker-compose up -d` to init mysql container

## Demo
### Create table
Execute `create-tables.sql`

### Create sample data
Execute `create-dummy-data.sql`

### Prepare data for testing query through/not through index
Execute `add-column.sql` to test query through/not through index

### Test query through/not through index
Execute each query in `query.sql` to test query through/not through index
#### Result
##### Through index
![alt text](images/through-index.png "Through index")
##### Not through index
![alt text](images/not-through-index.png "Not through index")

### Test lock row
Execute 2 queries in lock-example.sql at the same time using 2 sessions
#### Result 
![alt text](images/lock-row.png "Lock row")
