CREATE TABLE `customer` (
  `customer_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `zipcode` char(7),
  `address` varchar(255),
  `phone_number` varchar(13),
  `insert_date` datetime NOT NULL,
  PRIMARY KEY (`customer_id`)
);

CREATE TABLE `department` (
  `department_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`department_id`)
);

CREATE TABLE `staff` (
  `staff_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `department_id` int,
  PRIMARY KEY (`staff_id`)
);

CREATE TABLE `provider` (
  `provider_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`provider_id`)
);

CREATE TABLE `product` (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `price` int NOT NULL,
  PRIMARY KEY (`product_id`)
);

CREATE TABLE `hatchu` (
  `hatchu_id` int NOT NULL AUTO_INCREMENT,
  `provider_id` int NOT NULL,
  `staff_id` int NOT NULL,
  `juchu_id` int,
  `hatchu_date` datetime NOT NULL,
  `delivery_date` datetime NOT NULL,
  PRIMARY KEY (`hatchu_id`)
);

CREATE TABLE `hatchu_product` (
  `hatchu_product_id` int NOT NULL AUTO_INCREMENT,
  `hatchu_id` int NOT NULL,
  `product_id` int NOT NULL,
  `quantity` int NOT NULL,
  `discount` int NOT NULL DEFAULT 0,
  `tax_rate` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`hatchu_product_id`)
);

CREATE TABLE `juchu` (
  `juchu_id` int NOT NULL AUTO_INCREMENT,
  `customer_id` int NOT NULL,
  `juchu_date` datetime NOT NULL,
  `delivery_date` datetime NOT NULL,
  `staff_confirm_juchu_id` int NOT NULL,
  `staff_confirm_discount_id` int,
  PRIMARY KEY (`juchu_id`)
);

CREATE TABLE `juchu_product` (
  `juchu_product_id` int NOT NULL AUTO_INCREMENT,
  `juchu_id` int NOT NULL,
  `product_id` int NOT NULL,
  `quantity` int NOT NULL,
  `discount` int NOT NULL DEFAULT 0,
  `tax_rate` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`juchu_product_id`)
);
