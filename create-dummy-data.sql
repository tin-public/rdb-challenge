drop procedure if exists create_dummy_data;
drop function if exists random_int;

create function random_int(v_from int, v_to int) returns int return floor(v_from + rand() * (v_to - v_from));

delimiter #
create procedure create_dummy_data()
begin
  declare n_product int default 100;
  declare n_juchu int default  10000;
  declare j int default 1;
  declare jdate datetime default now() - interval 1000 day;

  declare i int default 1;
  truncate table `product`;
  while i <= n_product do
    insert into `product` (`product_id`, `name`, `price`) values (i, concat('product ',i), random_int(10, 100));
    set i = i + 1;
  end while;

  set i = 1;
  truncate table `juchu_product`;
  truncate table `juchu`;
  while i <= n_juchu do
    set jdate = jdate + interval random_int(1, 10) minute;
    insert into juchu (
      `juchu_id`,
      `customer_id`,
      `juchu_date`,
      `delivery_date`,
      `staff_confirm_juchu_id`,
      `staff_confirm_discount_id`
    ) values (
      i,
      random_int(1, 100),
      jdate,
      jdate + interval random_int(2, 3) day,
      random_int(1, 100),
      null
    );

    set j = 1;
    while j <= n_product do
      insert into `juchu_product` (
        `juchu_id`,
        `product_id`,
        `quantity`,
        `discount`,
        `tax_rate`
      ) values (
        i,
        j,
        random_int(1, 10),
        0,
        random_int(5, 10)
    );
      set j = j + 1;
    end while;
    set i = i + 1;
  end while;
end #

delimiter ;
call create_dummy_data;
