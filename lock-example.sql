-- Session 1
START TRANSACTION;
SELECT * FROM `product` WHERE `product_id` = 1 FOR UPDATE;
DO SLEEP(20);
COMMIT;

-- Session 2
START TRANSACTION;
UPDATE `product` set `price` = 10 where `product_id` = 1;
COMMIT;
